﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EasyWithTestUdpsender.Enviroment
{
    public class Config
    {

        #region Instance
        private static Config _instance;
        public static Config Instnace
        {
            get
            {
                if (_instance == null)
                    _instance = new Config();
                return _instance;
            }
        }
        #endregion

        public string ID { get; set; } = "127.0.0.1";
        public string PORT { get; set; } = "7777";
        public int LogCount { get; set; } = 500;


        public void SaveConfig(string id, string poart)
        {
            string path = System.Environment.CurrentDirectory + "\\config.json";
            using(StreamWriter sw = new StreamWriter(path))
            {
                var p = new Information { ID = id, PORT = poart };
                string jsonString = JsonConvert.SerializeObject(p);
                sw.Write(jsonString);
            }
        }

        public void LoadConfig()
        {
            string path = System.Environment.CurrentDirectory + "\\config.json";

            // 파일 없을 경우
            if (!File.Exists(path))
            {
                return;
            }

            using(StreamReader sr = new StreamReader(path))
            {
                string jsonString = sr.ReadToEnd();
                Information information = JsonConvert.DeserializeObject<Information>(jsonString);
                ID = information.ID;
                PORT = information.PORT;
            }

        }
    }

    public class Information
    {
        public string ID { get; set; } = "127.0.0.1";
        public string PORT { get; set; } = "7777";
    }
}
