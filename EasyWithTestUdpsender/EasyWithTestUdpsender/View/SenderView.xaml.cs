﻿using EasyWithTestUdpsender.Enviroment;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasyWithTestUdpsender.View
{
    /// <summary>
    /// SenderView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class SenderView : UserControl
    {
        public SenderView()
        {
            InitializeComponent();
            
            Config.Instnace.LoadConfig();
            IDTextBox.Text = Config.Instnace.ID;
            PortTextBox.Text = Config.Instnace.PORT;
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = IDTextBox.Text;
                var port = PortTextBox.Text;

                // (1) Id, Port 값 가져오기
                Config.Instnace.SaveConfig(id, port);

                // (1) UdpClient 객체 성성
                UdpClient cli = new UdpClient($"{id}", Int32.Parse(port));

                // (2) 데이타 송신
                string msg = SendTB.Text;
                byte[] datagram = Encoding.UTF8.GetBytes(msg);
                cli.Send(datagram, datagram.Length);

                // (3) UdpClient 객체 닫기
                cli.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }
    }
}
