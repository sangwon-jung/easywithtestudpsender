﻿using EasyWithTestUdpsender.Enviroment;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasyWithTestUdpsender.View
{
    /// <summary>
    /// ReceiverView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ReceiverView : UserControl
    {
        private Thread _receiverThread;
        private bool _isRunnigThread = true;
        private UdpClient _receiverUdpClient = null;

        public ReceiverView()
        {
            InitializeComponent();
            
            Config.Instnace.LoadConfig();
            IDTextBox.Text = Config.Instnace.ID;
            PortTextBox.Text = Config.Instnace.PORT;
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            _isRunnigThread = true;
            string id = null, port = null;
            id = IDTextBox.Text;
            port = PortTextBox.Text;
            Config.Instnace.SaveConfig(id, port);

            /// Open Message
            LogLV.Items.Add(new LogTable
            {
                CurrentTime = DateTime.Now.ToString(),
                Content = "***  Open  ***"
            });

            /// Thread Create
            _receiverThread = new Thread(new ThreadStart(delegate
            {
                ReceiverThread(id, port);
            }))
            {
                IsBackground = true
            };
            _receiverThread.Start();

            /// Button Hidden
            OpenButton.Visibility = Visibility.Hidden;
            CloseButton.Visibility = Visibility.Visible;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            /// Close Message
            LogLV.Items.Add(new LogTable
            {
                CurrentTime = DateTime.Now.ToString(),
                Content = "***  Close  ***"
            });

            /// Button Change
            OpenButton.Visibility = Visibility.Visible;
            CloseButton.Visibility = Visibility.Hidden;

            _isRunnigThread = false;
            _receiverUdpClient.Close();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void ReceiverThread(string id, string port)
        {
            try
            {
                // (1) UdpClient 객체 성성
                _receiverUdpClient = new UdpClient(Int32.Parse(port));

                // (2) 클라이언트 IP를 담을 변수 생성
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(id), 0);

                // (3) log list 생성
                List<LogTable> logTables = new List<LogTable>();

                while (_isRunnigThread)
                {
                    try
                    {
                        // (4) 데이타 수신
                        byte[] dgram = _receiverUdpClient.Receive(ref remoteEP);

                        // (5) UI Update
                        this.Dispatcher.BeginInvoke(
                            new Action(() =>
                            {
                                if (LogLV.Items.Count == 500)
                                {
                                    LogLV.Items.Clear();
                                }

                                LogLV.Items.Add(new LogTable
                                {
                                    CurrentTime = DateTime.Now.ToString(),
                                    Content = Encoding.UTF8.GetString(dgram)
                                });

                                var scrollViewer = GetScrollViewer(LogLV) as ScrollViewer;
                                if (scrollViewer != null)
                                {
                                    scrollViewer.ScrollToBottom();
                                }

                              
                            }));
                    }
                    catch (SocketException se)
                    {
                        break;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Scroll Bottom
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer

            if (o is ScrollViewer)
            { return o; }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result == null)
                {
                    continue;
                }
                else
                {
                    return result;
                }
            }
            return null;
        }


        public class LogTable
        {
            public string CurrentTime { get; set; }
            public string Content { get; set; }
        }

        
    }
}
