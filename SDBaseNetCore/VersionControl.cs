﻿using System;
using System.Globalization;
using System.Reflection;

namespace SDBaseNetCore
{
    public class VersionControl
    {
        public struct BuildInfo
        {
            public DateTime build_date { get; set; }
            public Version version { get; set; }

            public override string ToString()
            {
                return $"Version:{version}, Build:{version.Revision}, Date:{build_date}";
            }
        }

        public static BuildInfo build_info { get; private set; }
        public VersionControl(Assembly excuting_assembly)
        {
            build_info = new BuildInfo()
            {
                build_date = GetBuildDate(excuting_assembly),
                version = excuting_assembly.GetName().Version
            };

            Console.WriteLine();
        }

        private static DateTime GetBuildDate(Assembly assembly)
        {
            const string BuildVersionMetadataPrefix = "+imfine.sd";

            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            if (attribute?.InformationalVersion != null)
            {
                var value = attribute.InformationalVersion;
                var index = value.IndexOf(BuildVersionMetadataPrefix);
                if (index > 0)
                {
                    value = value.Substring(index + BuildVersionMetadataPrefix.Length);
                    if (DateTime.TryParseExact(value, "yyyyMMddHHmmss", CultureInfo.CurrentCulture, DateTimeStyles.None, out var result))
                    {
                        return result;
                    }
                }
            }

            return default;
        }
    }
}
