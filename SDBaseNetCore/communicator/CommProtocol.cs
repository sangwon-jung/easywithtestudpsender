﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



public class CommProtocol
{
    //exceptions 
    public const short ERR_KEY_NOT_EXIST = -30006;      // sw 추가
    public const short ERR_BOARD_NOT_EXIST = -30005;    // sw 추가
    public const short ERR_FAIL_DATATRANSFER = -30004;
    public const short ERR_FILE_NOT_EXIST = -30003;
    public const short ERR_DELETE_FILE = -30002;
    public const short ERR_HASH_NOT_MATCHED = -30002;
    public const short ERR_READ_DATA = -30001;


    //timesync error < -9100
    public const short ERR_TIMESYNC_ERROR = -9001;

    //knox error < -9000
    public const short ERR_KNOX_AUTHORIZATION = -9000;
    public const short ERR_KNOX_APP_INSTALL = -9001;


    //web error <-2000
    public const short ERR_WEB_AUTHORIZATION = -2000;
    public const short ERR_WEB_DBINFO_IS_NULL = -2001;
    public const short ERR_WEB_BODY_REQ_FORMAT = -2002;
    public const short ERR_WEB_ONE_OF_THE_REQ_PROP_IS_EMPTY = -2003;
    public const short ERR_INVALID_VALUE = -2004;
    public const short ERR_HTTP_STATUS_IS_NOT_OK = -2005;
    public const short ERR_HTTP_REQ_ERR = -2006;

    //web generic <-2050
    public const short ERR_404_NOT_FOUND = -2050;
    public const short ERR_FCM_EXCEPTION = -2051;

    //web user login <-2100
    public const short ERR_USER_EMAIL_ALREADY_EXISTS = -2102;
    public const short ERR_USER_NICK_ALREADY_EXISTS = -2103;
    public const short ERR_USER_NOT_EXISTS = -2104;
    public const short ERR_USER_IN_BLACKLIST = -2105;
    public const short ERR_USER_PASSWORD_NOT_MATCHED = -2106;

    

    //app error < 0
    public const short ERR_UNEXPECTED = -1001;
    public const short ERR_GENERIC = -1002;
    public const short ERR_CONNECTION = -1003;
    public const short ERR_SEND_MESSAGE = -1004;
    public const short ERR_FILESIZE = -1005;
    public const short ERR_FILE_CHG_NAME = -1006;
    public const short ERR_GFFILE_INFO_PARSE_FAIL = -1007;
    public const short ERR_FILE_NOT_EXISTS = -1008;
    public const short ERR_FILE_MD5_MISMATCH = -1009;
    public const short ERR_FILE_CAN_NOT_OPEN = -1010;
    public const short ERR_FILE_ID_EXCEPTION = -1011;
    public const short ERR_FILE_EXIST = -1012;
    public const short ERR_IO_EXCEPTION = -1013;
    public const short ERR_CANCEL = -1014;
    public const short ERR_PARSE_JSON_ERR = -1015;
    public const short ERR_INVALIDATE_DIR_PATH = -1016;
    public const short ERR_DATABASE_INSERT = -1017;
    public const short ERR_DATABASE_EXCEPTION = -1018;
    public const short ERR_CONNECITON_NOT_EXISTS = -1019;
    public const short ERR_DIR_NOT_EXISTS = -1020;
    public const short ERR_INFO_DATA = -1021;
    public const short ERR_IMEI_FORMAT_IS_INVALIDATE = -1022;
    public const short ERR_FILE_INVALID_EXCEPTION = -1023;
    public const short ERR_FILE_NOT_CREATED = -1024;
    public const short ERR_ALREADY_STARTED = -1025;
    public const short ERR_INVALID_FORMAT = -1026;
    public const short ERR_TOO_MUCH_DATA_SIZE = -1027;
    public const short ERR_NULLPOINTER = -1028;
    public const short ERR_NOT_RESPONDING = -1029;
    public const short ERR_EMPTY_DATA = -1030;

    public const short READY = 500;
    public const short NULL = 1000;
    public const short OK = 1001;

    public const short ECHO = 1050;
    public const short INTRODUCE = 1051;
    public const short REQ_FILE_TRANSFER = 1052;
    public const short REQ_TIMESYNC = 1053;
    public const short REQ_TIMECHECK = 1054;

    public const short CHK_FILETRANSFER_COMPLETE = 1055;
    public const short CHK_FILETRANSFER_ERR_FILE_SIZE_FAIL = 1056;
    public const short CHK_FILETRANSFER_ERR_SOCKET_ERROR = 1057;

    public const short MANUAL_TIME_CONTROL = 1058;

    public const short WELCOME = 1100;
    public const short TRACE_ON = 1101;
    public const short TRACE_OFF = 1102;
    public const short APP_UPDATE = 1103;
    public const short AUTO_ECHO_START = 1104;
    public const short AUTO_ECHO_STOP = 1105;
    public const short APP_AUTO_UPDATE = 1106;
    public const short KNOX_AUTO_UPDATE = 1107;
    public const short KIOSK_ON = 1108;
    public const short KIOSK_OFF = 1109;

    public const short START_TIMELINE = 1110;
    public const short STOP_TIMELINE = 1111;
    public const short RESUME_TIMELINE = 1112;

    public const short REQ_ID_CHECK = 1124;
    public const short CHANGE_ID = 1125;
    public const short KILL_SOCKET = 1126;
    public const short REBOOT = 1127;
    public const short DEVICE_SETTING = 1128;

    public const short CHANGE_SCREEN_SPACE = 1500;

    //report
    public const short BATTERY_REPORT = 1600;
    public const short RESOURCE_INTERGRITY_REPORT = 1601;
    public const short RESOURCE_FILELIST = 1602;
    public const short UPDATE_STATUS = 1603;

    public const short WEB_REQ_PULL_FILES = 2000;
    public const short WEB_CHK_DOWNLOAD_COMPLETE = 2001;
    public const short WEB_CHK_DOWNLOAD_ERROR = 2002;
    public const short DEBUG_DELETE_BUG_REPORT = 2003;
    public const short WEB_REQ_CANCEL_DOWNLOAD = 2004;
    public const short WEB_REQ_DATABASE = 2005;

    public const short CALIBRATION_IMAGE_VISIBLE = 2012;
    public const short UNITY_CONTENT_VISIBLE = 2013;
    public const short UNITY_DEBUG_VISIBLE = 2014;
    public const short UNITY_PARAMS = 2015;

    //messaging
    public const short DATA_POST_MESSAGE = 2100;
    public const short DATA_POST_MESSAGE_RECEIVE = 2101;

    //screen
    public const short SHOW_COLOR_SCREEN = 2500;
    public const short CHANGE_SCREEN_BRIGHTNESS = 2501;
    public const short SCREEN_ORIENTATION_MODE = 2502;
    public const short CONTENTS_ROTATION_MODE = 2503;

    //network
    public const short SET_STATIC_SSID = 2600;
    public const short SET_STATIC_IP = 2601;


    //registration
    public const short REGISTRATION = 3000;
    public const short UNREGISTRATION = 3001;
    public const short ERR_DEVICE_IMEI_EXIST = 3002;
    public const short CHANGE_ALIAS = 3003;
    public const short GET_IMEI = 3004;


    // setting
    public const short SHOW_CLOCK = 4000;
    public const short SHOW_SETTING = 4001;
    public const short KILL_APPS = 4002;
    public const short SYSTEM_TIME = 4003;

    //contents
    public const short CONTENT_STATUS = 5000;
    public const short CONTENT_STATUS_NEW = 5001;
    public const short PUSH_NOTIFICATION = 5003;
    public const short GXB_ABUSING_USER_CONTROL = 5004;
    public const short GXB_GET_VIDEO_SCREEN_DATA = 5005;
    public const short GXB_CONTENT_CHANGE = 5006;

    //File Transfer , File Info
    public const short FILE_TRANSFER = 6001;
    public const short FILE_INFO = 6002;
    public const short FILE_WRITER = 6003;
    public const short FILE_READER = 6004;
    public const short REQ_SHARE_FILES = 6005;
    public const short GET_FILE = 6006;

    //screen streaming 8900
    public const short START_SCREEN_STREAM = 8900;
    public const short STOP_SCREEN_STREAM = 8901;

    //knox 9000~
    public const short KNOX_APP_UNINSTALL = 9000;
    public const short KNOX_APP_INSTALL = 9001;
    public const short KNOX_STATIC_IP_MODE = 9002;
    public const short KNOX_STATIC_SSID_MODE = 9003;
    public const short KNOX_GPS_MODE = 9004;
    public const short KNOX_STORE_MODE = 9005;
    public const short KNOX_REBOOT_DEVICE = 9006;
    public const short KNOX_WIFI_MODE = 9007;
    public const short KNOX_KIOSK_MODE = 9008;
    public const short KNOX_AUTO_SHUTDOWN_MODE = 9009;
    public const short KNOX_SOFTWARE_UPDATE_MODE = 9010;
    public const short KNOX_APP_START = 9011;
    public const short KNOX_APP_STOP = 9012;
    public const short KNOX_ETHERNET_MODE = 9013;
    public const short KNOX_DEVICE_SETTING = 9014;
    public const short KNOX_SCREEN_ON_OFF_CTRL = 9015;
    public const short KNOX_AUTO_STARTUP = 9016;
    public const short KNOX_DEACTIVE = 9017;

    //system
    public const short AWAKE_DEVICE = 10000;
    public const short RESET_CONNECTION = 10001;
    public const short DEVELOPER_CODE = 10002;

    public const short TEST_COLOR_PLAY = 13000;
    public const short TEST_VIDEO_PLAY = 13001;
    public const short SHOW_TIMESYNC_CLOCK_VIEW = 13100;

    //tile gallery
    public const short TILE_GALLERY_IMAGE = 20000;
    public const short TILE_GALLERY_VIDEO = 20001;
    public const short TILE_GALLERY_COLOR = 20002;
    public const short TILE_GALLERY_GET_BOXES = 20003;

    //IMDataManager
    public const short IMDATA_StoC = 30000;
    public const short IMDATA_CtoS = 30001;
    public const short IMDATA_DELETE_FILE = 30002;
    public const short IMDATA_GETFOLDER_INFO = 30003;
    public const short IMDATA_FILE_EXIST_CHECK = 30004;
    public const short IMDATA_TRANSFER_SPEED_CHECK = 30005;

    private static Dictionary<short, string> ProtocolDatas;
    static CommProtocol()
    {
        ProtocolDatas = new Dictionary<short, string>();
        var a = typeof(CommProtocol).GetFields();
        foreach (var item in typeof(CommProtocol).GetFields())
        {
            if (item.FieldType != typeof(Int16))
                continue;

            //if (item.FieldType != false) continue;

            try
            {
                string name = item.Name;
                object value = item.GetValue(null);
                ProtocolDatas.Add((short)value, name);
            }
            catch
            {
            }
        }
    }
    
    public static Dictionary<short , string> GetAllProtocols()
    {
        return ProtocolDatas;
    }


    public static string Parse(short value)
    {
        if (ProtocolDatas.ContainsKey(value) == false)
            return null;

        return ProtocolDatas[value];
    }

}

