﻿using System;
using System.Collections.Generic;
using System.Text;

public class SCGlobal
{
    //------------------------------------------------------------------  TIME
    #region  TIME

    static DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

    public static long UnixTimeFromNow()
    {
        TimeSpan diff = DateTime.Now.ToUniversalTime() - origin;
        double time = Math.Truncate(diff.TotalSeconds * 1000);
        return (long)time;
    }

    public static long ToUnixTime(DateTime value)
    {
        TimeSpan diff = value - origin;
        double time = Math.Truncate(diff.TotalSeconds * 1000);
        return (long)time;
    }

    public static DateTime UnixTimeStampToDateTimeNow()
    {
        // Unix timestamp is seconds past epoch
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddMilliseconds(UnixTimeFromNow()).ToLocalTime();
        return dtDateTime;
    }

    public static DateTime UnixTimeStampToDateTime(long time)
    {
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddMilliseconds(time).ToLocalTime();
        return dtDateTime;
    }

    public static DateTime UnixTimeStampToUniversalDateTime(long time)
    {
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddMilliseconds(time).ToUniversalTime();
        return dtDateTime;
    }

    #endregion


    public static string GetExcutionPath(string path)
    {
        var b = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        return System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
            , path);
    }
}

public class CheckedSDBaseToNetCore : Attribute
{
    private string v;

    public CheckedSDBaseToNetCore()
    {

    }
    public CheckedSDBaseToNetCore(string v)
    {
        this.v = v;
    }

}